#-----------------------------------------------------------------------------
# default installation target for applications
#-----------------------------------------------------------------------------
contains(TEMPLATE, app) {
    target.path  = $${INSTALL_PREFIX}/bin
    INSTALLS    += target
    message("====")
    message("==== INSTALLS += target")
}

# End of File
